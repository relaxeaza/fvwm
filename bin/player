#!/usr/bin/env bash

function is_playing () {
    status=$(mpc | head -2 | tail -1 | cut -d ' ' -f1)

    if [[ $status == '[playing]' ]]; then
        return 0
    fi

    return 1
}

function update_label () {
    if is_playing; then
        FvwmCommand 'SendToModule Panel-Player-Control ChangeButton toggle Icon icons/panel/pause.png'
    else
        FvwmCommand 'SendToModule Panel-Player-Control ChangeButton toggle Icon icons/panel/play.png'
    fi
}

function queued_songs () {
    playlist_count=$(mpc playlist | wc -l)

    if [[ $playlist_count -eq 0 ]]; then
        return 1
    fi

    return 0
}

if [[ $1 == --toggle ]]; then
    if queued_songs; then
        mpc toggle
    else
        FvwmCommand 'RaiseOrOpen cantata cantata'
    fi

    exit 0
fi

if [[ $1 == --prev ]]; then
    if queued_songs; then
        mpc prev
    else
        FvwmCommand 'RaiseOrOpen cantata cantata'
    fi

    exit 0
fi

if [[ $1 == --next ]]; then
    if queued_songs; then
        mpc next
    else
        FvwmCommand 'RaiseOrOpen cantata cantata'
    fi

    exit 0
fi

if [[ $1 == --update-label ]]; then
    if ! pidof mpd &>/dev/null; then
        echo 'mpd not running.' >&2

        exit 1
    fi

    update_label

    exit 0
fi

if [[ $1 == --playlist-menu ]]; then
    menu_songs_limit=15

    playlist=$(mpc playlist)

    if [[ -z $playlist ]]; then
        FvwmCommand 'RaiseOrOpen cantata cantata'

        exit 0
    fi

    playlist_count=$(wc -l <<< $playlist)
    menu_songs_limit_half=$((($menu_songs_limit - 1) / 2))
    current_song=$(mpc current)

    if [[ $current_song ]]; then
        song_position=$(grep "^$current_song$" --line-number <<< $playlist | cut -d ':' -f1)

        if (( $song_position < $menu_songs_limit_half )); then
            before_context=$song_position
            after_context=$(($menu_songs_limit - $song_position))
            song_index=1
        elif (( $playlist_count - $song_position < $menu_songs_limit_half )); then
            after_context=$(($playlist_count - $song_position))
            before_context=$(($menu_songs_limit - $after_context))
            song_index=$(($song_position - $before_context))
        else
            before_context=$menu_songs_limit_half
            after_context=$menu_songs_limit_half
            song_index=$(($song_position - $before_context))
        fi

        sliced_playlist=$(grep --before-context $before_context --after-context $after_context "^$current_song$" <<< $playlist)
    else
        song_index=1
        sliced_playlist=$(head -$menu_songs_limit <<< $playlist)
    fi

    echo 'AddToMenu PlayerQueueMenu "MPD Playlist" Title Top'

    while read song; do
        if [[ $current_song && $current_song == $song ]]; then
            echo "AddToMenu PlayerQueueMenu \"$song%icons/panel/active.png%\" Exec mpc play $song_index"
        else
            echo "AddToMenu PlayerQueueMenu \"$song\" Exec mpc play $song_index"
        fi

        song_index=$(($song_index + 1))
    done <<< $sliced_playlist

    echo 'AddToMenu PlayerQueueMenu "" Nop'
    echo 'AddToMenu PlayerQueueMenu "Clear playlist" Exec mpc clear'

    exit 0
fi

if [[ $1 = --daemon ]]; then
    function daemon () {
        if ! hash mpc 2>/dev/null; then
            echo 'mpc not installed.' >&2

            return 0
        fi

        if ! hash mpd 2>/dev/null; then
            echo 'mpd not installed.' >&2

            return 0
        fi

        if ! pidof mpd &>/dev/null; then
            return 1
        fi

        # update_label

        while mpc idle player &>/dev/null; do
            update_label
        done

        return 1
    }

    until daemon; do
        sleep 5
    done
fi
