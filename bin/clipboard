#!/usr/bin/env bash

if [[ $1 == --action ]] && [[ -f /tmp/clipboard-action ]]; then
    FvwmCommand 'Clipboard-Action-Hide'

    info=$(< /tmp/clipboard-action)
    id=$(cut -d '|' -f1 <<< $info)
    value=$(cut -d '|' -f2 <<< $info)

    case $id in
        mpv) mpv "$value" ;;
        booru) echo "$value" >> $DROPBOX/NOTES ;;
        pixiv) echo "$value" >> $DROPBOX/NOTES ;;
    esac
fi

if [[ $1 == --daemon ]]; then
    function daemon () {
        while $FVWM_USERDIR/bin/clipnotify -s clipboard; do
            url=$(xclip -selection clipboard -out)

            if [[ -z $url || $url == $last ]]; then
                continue
            fi

            last=$url

            echo "$last"

            case $url in
                https://www.youtube.com/watch\?v=*|https://youtu.be/*|https://invidio.us/watch\?v=*)
                    echo "mpv|$url" > /tmp/clipboard-action
                    FvwmCommand "Clipboard-Action-Show 'Open youtube link' 'icons/apps/mpv.png' '30'"
                    ;;

                https://gelbooru.com/index.php\?page=post\&s=view\&id=[0-9]*|https://gelbooru.com/index.php\?page=post\&s=list\&tags=*|https://gelbooru.com/index.php\?page=post\&s=list\&md5=*)
                    echo "booru|$url" > /tmp/clipboard-action
                    FvwmCommand "Clipboard-Action-Show 'Save booru link' 'icons/services/booru.png' '31'"
                    ;;

                https://danbooru.donmai.us/posts/[0-9]*)
                    echo "booru|$url" > /tmp/clipboard-action
                    FvwmCommand "Clipboard-Action-Show 'Save booru link' 'icons/services/booru.png' '31'"
                    ;;

                https://www.pixiv.net/en/artworks/[0-9]*)
                    echo "pixiv|$url" > /tmp/clipboard-action
                    FvwmCommand "Clipboard-Action-Show 'Save pixiv link' 'icons/services/pixiv.png' '31'"
                    ;;

                *) continue;;
            esac
        done

        return 1
    }

    until daemon; do
        sleep 2
    done
fi
