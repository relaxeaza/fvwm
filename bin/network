#!/usr/bin/env bash

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --update-label) update_label_flag=1 ;;
        --speed-monitor) start_speed_monitor=1 ;;
        --set-env) set_env=1 ;;
        --notify) show_notif=1 ;;
    esac;
    shift;
done

if [[ $set_env ]]; then
    export FVWM_USERDIR=$HOME/.fvwm
    export PATH=/usr/bin

    function notify () {
        DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus setpriv --euid=1000 notify-send "$@"
    }
else
    function notify () {
        notify-send "$@"
    }
fi

function get_active_connection_type () {
    local ignore_interface=$1

    while read connection; do
        local connection_name=$(awk -F '  +' '{print $1}' <<< $connection)
        local connection_type=$(awk -F '  +' '{print $3}' <<< $connection)
        local connection_interface=$(awk -F '  +' '{print $4}' <<< $connection)

        if [[ $connection_interface != $ignore_interface ]]; then
            local found=1
            break
        fi
    done < <(nmcli connection show --active | tail +2)

    if [[ $found ]]; then
        active_connection_name=$connection_name
        active_connection_type=$connection_type
        active_connection_interface=$connection_interface
    fi
}

function get_local_ip () {
    ip route show scope link dev $active_connection_interface | awk '{print $5}'
}

function update_label () {
    get_active_connection_type $1

    case $active_connection_type in
        ethernet)
            label='Ethernet ON'
            icon='network-ethernet'
            notify_icon='connected'
            ;;
        wifi)
            label='Wireless ON'
            icon='network-wifi'
            notify_icon='connected'
            ;;
        bluetooth)
            label='Bluetooth ON'
            icon='network-bluetooth'
            notify_icon='connected'
            ;;
        *)
            label='Ethernet OFF'
            icon='network-disconnected'
            notify_icon='disconnected'
            ;;
    esac

    FvwmCommand "SendToModule Panel-Network ChangeButton label Title '$label'"
    FvwmCommand "SendToModule Panel-Network ChangeButton label Icon 'icons/panel/$icon.png'"

    if [[ $show_notif ]]; then
        notify --app-name network "$label" --icon "$notify_icon" --urgency low --expire-time 2000
    fi
}

if [[ $start_speed_monitor ]]; then
    if [[ -f /tmp/fvwm-speed-monitor-pid ]]; then
        pid=$(< /tmp/fvwm-speed-monitor-pid)

        if ps $pid >/dev/null; then
            kill $pid
        fi

        FvwmCommand 'KillModule FvwmButtons Speed-Monitor -transient'
        # FvwmCommand 'SendToModule Panel-Network ChangeButton label Icon icons/panel/arrow-left.png'

        rm /tmp/fvwm-speed-monitor-pid

        exit 0
    fi

    get_active_connection_type

    FvwmCommand 'Module FvwmButtons Speed-Monitor'
    # FvwmCommand 'SendToModule Panel-Network ChangeButton label Icon icons/panel/arrow-right.png'

    if [[ $active_connection_name ]]; then
        echo $$ > /tmp/fvwm-speed-monitor-pid

        FvwmCommand "Schedule 100 SendToModule Speed-Monitor ChangeButton name Title '$active_connection_name'"
        FvwmCommand "Schedule 100 SendToModule Speed-Monitor ChangeButton local_ip Title '$(get_local_ip)'"

        ifstat $active_connection_interface --nooutput
        sleep 0.3

        while true; do
            stats=$(ifstat $active_connection_interface | grep $active_connection_interface)
            down=$(awk '{print $6}' <<< $stats)
            up=$(awk '{print $8}' <<< $stats)

            down_quant=${down: -1}
            up_quant=${up: -1}

            if [[ $down_quant == 'M' ]]; then
                down=$(( ${down%M} / 1024 ))
            elif [[ $down_quant == 'K' ]]; then
                down=${down%K}
            else
                down=$(($down / 1024))
            fi

            if [[ $up_quant == 'M' ]]; then
                up=$(( ${up%M} / 1024 ))
            elif [[ $up_quant == 'K' ]]; then
                up=${up%K}
            else
                up=$(($up / 1024))
            fi

            FvwmCommand "SendToModule Speed-Monitor ChangeButton down Title '$down KB/s'"
            FvwmCommand "SendToModule Speed-Monitor ChangeButton up Title '$up KB/s'"

            sleep 1
        done
    else
        echo button > /tmp/fvwm-speed-monitor-pid
        FvwmCommand 'Schedule 100 SendToModule Speed-Monitor ChangeButton name Title "Not connected"'
    fi

    exit 0
fi

if [[ $update_label_flag ]]; then
    update_label

    exit 0
fi
