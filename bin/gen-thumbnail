#!/usr/bin/env bash

# to include the program icon in the thumbnail it needs to be defined
# with Style "program_id" MiniIcon /path/to/icon.png

while [[ "$#" -gt 0 ]]; do case $1 in
    --wid) wid=$2; shift ;;
    --icon) icon=$2; shift ;;
    --icon-gravity) icon_gravity="$2"; shift ;;
    --add-shadow) add_shadow=1 ;;
    --size) size=$2; shift ;;
esac; shift; done

if ! hash maim 2>/dev/null; then
    echo "$0: maim not installed" >&2

    exit 1
fi

if ! hash convert 2>/dev/null; then
    echo "$0: imagemagick not installed" >&2

    exit 1
fi

maim --window $(($wid)) --format=jpg --quality=10 | convert -define jpg:size=$size -thumbnail $size^ -gravity NorthWest jpg:- -extent $size /tmp/fvwm-icon-$wid.png

if [[ $icon ]]; then
    if ! [[ -f $icon ]]; then
        classname=$(xprop WM_CLASS -id $(($wid)) | cut -d '"' -f 4 | tr '[:upper:]' '[:lower:]')
        auto_icon="$FVWM_USERDIR/images/icons/apps/$classname.png"

        if [[ -f $auto_icon ]]; then
            icon=$auto_icon
        fi
    fi

    if ! [[ $icon_gravity ]]; then
        icon_gravity='center'
    fi

    icon_gravity=$(tr '[:upper:]' '[:lower:]' <<< $icon_gravity)

    case $icon_gravity in
        northwest|northeast) position_adjust='+1+2' ;;
        north) position_adjust='+0+2' ;;
        west|east) position_adjust='+1+0' ;;
        center) position_adjust='+0+0' ;;
        southwest|southeast) position_adjust='+1+1' ;;
        south) position_adjust='+0+1' ;;
        *) position_adjust='+1+2' ;;
    esac

    if [[ $add_shadow ]]; then
        convert "$icon" \( +clone -background black -shadow 100x1+1+1 \) +swap -background none -layers merge png:- |
        composite -gravity $icon_gravity -geometry $position_adjust png:- /tmp/fvwm-icon-$wid.png /tmp/fvwm-icon-$wid.png
    else
        composite -gravity $icon_gravity -geometry $position_adjust "$icon" /tmp/fvwm-icon-$wid.png /tmp/fvwm-icon-$wid.png
    fi
fi
